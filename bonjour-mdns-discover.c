
#include <stdint.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include <net/if.h>

#include <dns_sd.h>
#include <json-c/json.h>

#define DEBUG_OUT 0
#define LOG_CALLBACK 0

static DNSServiceRef client = NULL;

void send_output(const char *text)
{
#if DEBUG_OUT
  printf("%d: '%s'\n", (int)strlen(text), text);
  fflush(stdout);
#else
  int len = strlen(text);
  write(STDOUT_FILENO, &len, 4);
  write(STDOUT_FILENO, text, len);
#endif
}

// Read 4 bytes,
// Alloc memory for length
// Read length into memory
// Return true
// Return false on EOF
int read_object(char **out)
{
  int len;
  int ret = read(STDIN_FILENO, &len, 4);
  if (ret != 4)
    return 0;

  char *buffer = malloc(len + 1);
  if (!buffer)
    return 0;

  buffer[len] = 0;
  char *bp = buffer;
  while (len) {
    int ret = read(STDIN_FILENO, bp, len);
    if (ret <= 0) {
      free(buffer);
      return 0;
    }
    bp += ret;
    len -= ret;
  }
  *out = buffer;
  return 1;
}

int WaitOnClient(DNSServiceRef rc)
{
  int fd = DNSServiceRefSockFD(rc);
  fd_set readfds;
  struct timeval tv;

  FD_ZERO(&readfds);
  tv.tv_sec = 1; //timeOut
  tv.tv_usec = 1000;
  FD_SET(fd, &readfds);
  int ret = select(fd + 1, &readfds, (fd_set *)NULL, (fd_set *)NULL, &tv);
  // > 0 do the thing
  // = 0 timedout
  // < 0 error
  return ret;
}

static void
address_reply(DNSServiceRef sdref, DNSServiceFlags flags,
              uint32_t ifIndex, DNSServiceErrorType errorCode,
              const char *fullname,
              uint16_t rrtype, uint16_t rrclass,
              uint16_t rdlen, const void *rdata,
              uint32_t ttl, void *context)
{
#if LOG_CALLBACK
  char b[IF_NAMESIZE];
  fprintf(stderr, "%s: fl %x if %s, e %d, [%s]\n", __func__, flags, if_indextoname(ifIndex, b), errorCode, fullname);
#endif

  volatile unsigned int *done = (volatile unsigned int *) context;
  *done = *(volatile unsigned int *)rdata;
}

struct resolve_context {
  const char *srv_name;
  const char *srv_type;
  volatile int done;
};

static void
resolve_reply(DNSServiceRef sdref, const DNSServiceFlags flags,
              uint32_t ifIndex, DNSServiceErrorType errorCode,
              const char *fullname, const char *hostname, uint16_t opaqueport,
              uint16_t txtLen, const unsigned char *txtRecord,
              void *context)
{
  uint16_t port = htons(opaqueport);
  struct resolve_context *ctx = context;
  struct json_object *jo, *tx;
  int i;
  int32_t  err = 0;
  DNSServiceRef rc = 0;
  volatile struct in_addr address = {0};

#if LOG_CALLBACK
  char b[IF_NAMESIZE];
  fprintf(stderr, "%s: fl %x if %s, e %d, [%s]\n", __func__, flags, if_indextoname(ifIndex, b), errorCode, hostname);
#endif

  err = DNSServiceQueryRecord(&rc, 0, ifIndex, hostname, kDNSServiceType_A, kDNSServiceClass_IN,
                              address_reply, (void *)&address);
  if (!err && rc) {
    do {
      WaitOnClient(rc);
      err = DNSServiceProcessResult(rc);
    } while (!err && !address.s_addr); // Assumes address will not be 0.0.0.0
    DNSServiceRefDeallocate(rc);
  }

  jo = json_object_new_object();
  json_object_object_add(jo, "type", json_object_new_string("add"));
  json_object_object_add(jo, "srv", json_object_new_string_len(ctx->srv_type, strlen(ctx->srv_type) - 1));
  json_object_object_add(jo, "service", json_object_new_string(ctx->srv_name));
  json_object_object_add(jo, "hostname", json_object_new_string(hostname));
  json_object_object_add(jo, "address", json_object_new_string(inet_ntoa(address))); // need to resolve address
  json_object_object_add(jo, "port", json_object_new_int(port));
  tx = json_object_new_object();

  int cnt = TXTRecordGetCount(txtLen, txtRecord);
  char key[256];
  uint8_t len = 0;
  const void *val = 0;
  for (i = 0; i < cnt; i++) {
    TXTRecordGetItemAtIndex(txtLen, txtRecord, i, sizeof key, key, &len, &val);
    //* For keys with no value,        *val is set to NULL and *len is zero.
    //* For keys with empty value,     *val is non-NULL    and *len is zero.
    //* For keys with non-empty value, *val is non-NULL    and *len is non-zero.
    if (val) {
      if (len)
        json_object_object_add(tx, key, json_object_new_string_len(val, len));
      else
        json_object_object_add(tx, key, (struct json_object *)0);
    }
  }

  json_object_object_add(jo, "txt", tx);
  char const *s = json_object_to_json_string_ext(jo, JSON_C_TO_STRING_PLAIN);
  send_output(s);
  json_object_put(jo);

  ctx->done = 1;
}

static void
browse_reply(DNSServiceRef sdref, const DNSServiceFlags flags,
             uint32_t iface, DNSServiceErrorType errorCode,
             const char *service, const char *type, const char *domain,
             void *context)
{
#if LOG_CALLBACK
  char b[IF_NAMESIZE];
  fprintf(stderr, "%s: fl %x if %s, e %d, [%s]\n", __func__, flags, if_indextoname(iface, b), errorCode, service);
#endif

  if (flags & kDNSServiceFlagsAdd) {
    // Adding
    // Resolve service name and host name
    struct resolve_context ctx = {service, type, 0};
    DNSServiceRef rc = NULL;
    int32_t err = DNSServiceResolve(&rc, 0, iface, service, type, domain,
                                resolve_reply, (void *)&ctx);
    if (!err && rc) {
      do {
        WaitOnClient(rc);
        err = DNSServiceProcessResult(rc);
      } while (!err && !ctx.done);
      DNSServiceRefDeallocate(rc);
    }
  } else {
    // Removing
    // Don't need to resolve anything here.  Just send the "remove" message.
    struct json_object *jo = json_object_new_object();
    json_object_object_add(jo, "type", json_object_new_string("remove"));
    json_object_object_add(jo, "srv", json_object_new_string_len(type, strlen(type) - 1));
    json_object_object_add(jo, "service", json_object_new_string(service));
    char const *s = json_object_to_json_string_ext(jo, JSON_C_TO_STRING_PLAIN);
    send_output(s);
    json_object_put(jo);
  }
}

static bool stopNow = false;

#define max(a, b) ((a > b) ? a : b)

static void HandleEvents(void)
{
  int dns_sd_fd = client ? DNSServiceRefSockFD(client) : -1;
  int nfds = dns_sd_fd + 1;
  fd_set readfds;
  struct timeval tv;
  int result;

  while (!stopNow) {
    FD_ZERO(&readfds);
    FD_SET(STDIN_FILENO, &readfds);

    if (client)
      FD_SET(dns_sd_fd, &readfds);

    tv.tv_sec = 3; //timeOut
    tv.tv_usec = 0;

    result = select(nfds, &readfds, (fd_set *)NULL, (fd_set *)NULL, &tv);
    if (result > 0) {
      if (FD_ISSET(STDIN_FILENO, &readfds)) {
        // Data is available on stdin, so stop browsing
        return;
      }
      if (client && FD_ISSET(dns_sd_fd, &readfds)) {
        int32_t err = DNSServiceProcessResult(client);
        if (err) {
	  stopNow = true;
	}
      }
    } else if (result == 0) {
      /* timeout */;
    } else {
      if (errno != EINTR)
        stopNow = true;
    }
  }
}

static void
exit_handler(int sig)
{
  return exit(1);
}


int main()
{
  setenv("AVAHI_COMPAT_NOWARN", "1", 1);

  struct sigaction sa;
  sa.sa_flags = 0;
  sigemptyset(&sa.sa_mask);
  sa.sa_handler = exit_handler;
  if (sigaction(SIGINT, &sa, NULL) == -1)
    return 1;
  if (sigaction(SIGTERM, &sa, NULL) == -1)
    return 1;

  uint32_t flags = 0; // DNSServiceFlags
  uint32_t iface = 0; // kDNSServiceInterfaceIndexAny;
  char *buffer = 0;
  while (read_object(&buffer)) {
    struct json_object *parsed_json = json_tokener_parse(buffer);
    if (client)
      DNSServiceRefDeallocate(client);

    // DNSServiceErrorType
    int32_t err = DNSServiceBrowse(&client, flags, iface, json_object_get_string(parsed_json),
                               "local", browse_reply, NULL);
    if (!err)
      HandleEvents();
    json_object_put(parsed_json);

    free(buffer);
  }
  return 0;
}
