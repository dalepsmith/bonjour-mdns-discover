
APP := bonjour-mdns-discover
all: $(APP)

# apt install libjson-c-dev
JSON := $$(pkg-config json-c --libs)

# apt install libavahi-compat-libdnssd-dev
COMPAT := $$(pkg-config avahi-compat-libdns_sd --libs)

# also:
# apt install pkgconf|pkg-config

clean:
	rm -f $(APP) *.o

# apt install libavahi-compat-libdnssd-dev
$(APP): $(APP).c
	gcc -o $(APP) -Wall $(APP).c $(COMPAT) $(JSON) -pthread


USRDIR := $$HOME/.mozilla/native-messaging-hosts
install-local: $(APP) mdns.discover.firefox.json.in
	mkdir -p $(USRDIR)
	sed -e "s|@DIR@|$(USRDIR)|" mdns.discover.firefox.json.in > $(USRDIR)/mdns.discover.json
	cp -f $(APP) $(USRDIR)/

uninstall-local:
	$(RM) -f $(USRDIR)/mdns.discover.json
	$(RM) -f $(USRDIR)/$(APP)

LIBDIR = /usr/lib/mozilla/native-messaging-hosts
install: $(APP) mdns.discover.firefox.json.in
	mkdir -p $(LIBDIR)
	sed -e "s|@DIR@|$(LIBDIR)|" mdns.discover.firefox.json.in > $(LIBDIR)/mdns.discover.json
	cp -f $(APP) $(LIBDIR)/

uninstall:
	$(RM) -f $(LIBDIR)/$(APP)
	$(RM) -f $(LIBDIR)/mdns.discover.json
