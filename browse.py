#!/usr/bin/env python

import select
import socket
import struct
import sys
import pybonjour
import json
import threading
import signal
import os

if sys.platform == "win32":
    import os, msvcrt
    msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)

timeout  = 3
resolved = []
queried  = []
done = []
debug_out = False

# Helper function that sends a message to the webapp.
def send_message(message):
    if debug_out:
        sys.stdout.write(message)                        # Write the message itself.
        sys.stdout.write("\n")
    else:
        sys.stdout.buffer.write(struct.pack('I', len(message))) # Write message size.
        sys.stdout.write(message)                        # Write the message itself.
    sys.stdout.flush()

def query_record_callback(sdRef, flags, interfaceIndex, errorCode, fullname, rrtype, rrclass, rdata, ttl):
    if errorCode == pybonjour.kDNSServiceErr_NoError:
        queried.append(socket.inet_ntoa(rdata))

def resolve_hostname(iface, hostname):
    address  = ''
    query_sdRef = pybonjour.DNSServiceQueryRecord(interfaceIndex = iface,
                                                  fullname = hostname,
                                                  rrtype = pybonjour.kDNSServiceType_A,
                                                  callBack = query_record_callback)

    try:
        while not queried:
            ready = select.select([query_sdRef], [], [], timeout)
            if query_sdRef not in ready[0]:
                break
            pybonjour.DNSServiceProcessResult(query_sdRef)
        else:
            address = queried.pop()
    finally:
        query_sdRef.close()
    return address


def resolve_callback(sdRef, flags, interfaceIndex, errorCode, fullname, hosttarget, port, txtRecord):
    if errorCode == pybonjour.kDNSServiceErr_NoError:
        text = pybonjour.TXTRecord.parse(txtRecord)
        resolved.append({
            'hostname': hosttarget,
            'address': resolve_hostname(interfaceIndex, hosttarget),
            'port': port,
            'txt': { k: v for k, v in iter(text) }
        })


def browse_callback(sdRef, flags, interfaceIndex, errorCode, serviceName,
                    regtype, replyDomain):
    if errorCode != pybonjour.kDNSServiceErr_NoError:
        return

    if not (flags & pybonjour.kDNSServiceFlagsAdd):
        send_message(json.dumps({'type': 'remove', 'service': serviceName}))
        return

    resolve_sdRef = pybonjour.DNSServiceResolve(0,
                                                interfaceIndex,
                                                serviceName,
                                                regtype,
                                                replyDomain,
                                                resolve_callback)

    try:
        while not resolved:
            ready = select.select([resolve_sdRef], [], [], timeout)
            if resolve_sdRef not in ready[0]:
                break
            pybonjour.DNSServiceProcessResult(resolve_sdRef)
        else:
            stuff = resolved.pop()
            stuff['type'] = 'add'
            stuff['srv'] = regtype[:-1]
            stuff['service'] = serviceName
            #send_message(json.dumps(stuff, indent=5, separators=(',',': ')))
            send_message(json.dumps(stuff))
    finally:
        resolve_sdRef.close()

# Main thread. Browse for LXI services and exit when done is true
def browse_thread(service_type, stop):
    browse_sdRef = pybonjour.DNSServiceBrowse(regtype = service_type,
                                              callBack = browse_callback)
    try:
        while not done and not stop():
            ready = select.select([browse_sdRef], [], [], timeout)
            if browse_sdRef in ready[0]:
                pybonjour.DNSServiceProcessResult(browse_sdRef)
    finally:
        browse_sdRef.close()

# Read and from stdin and exit when stdin is EOF.
# Read 4 byte length then length bytes JSON
def wait_on_stdin():
    text_length_bytes = sys.stdin.buffer.read(4)
            
    if len(text_length_bytes) < 4:
        done.append(True)
        sys.exit()

    text_length = struct.unpack('i', text_length_bytes)[0]
    return json.loads(sys.stdin.buffer.read(text_length).decode('utf-8'))

def signal_handler(signum, frame):
    os._exit(0)

if __name__ == '__main__':
    debug_out = len(sys.argv) != 3
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    if debug_out:
        svc_type = "_http._tcp"
    else:
        svc_type = wait_on_stdin()
    while True:
        stopping = False
        thread = threading.Thread(target=browse_thread,
                                  args=(svc_type, lambda: stopping))
        thread.start()
        svc_type = wait_on_stdin()
        stopping = True
        thread.join()
